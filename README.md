# Gemeenschappelijk werken met git en GitLab

Dit project kan gebruikt worden als start van een CI/CD training, met name hoe het voortbrengingsproces er vanuit ontwikkelaars perspectief er uitziet. Door dit project te Forken (speciaal soort kopieren doormiddel van een afsplitsing), kan je de opdrachten uitvoeren zonder het basismateriaal onbruikbaar te maken.

Als eerste stap gaan we dus een fork maken van dit project:

![](Video/Fork_a_project.mp4)